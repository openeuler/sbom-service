# sbom-service

#### 介绍
以服务化方式提供SBOM工具链解决方案。提供一站式服务：软件成分解析、协同数据卷积、安全合规分析、SBOM导入导出、漏洞排查、直接/传递性依赖查询等；并解耦于任一SBOM协议格式。

#### 软件架构
软件架构说明


#### 安装教程

docker运行，见使用说明。


#### 使用说明

* 一、[SBOM介绍](doc/sbom/whatIsSbom.md)
* 二、[SBOM Service介绍](doc/sbom/sbomService.md)
* 三、[SBOM数据字段](doc/sbom/sbomFields.md)
* 四、[SBOM Service服务框架](doc/module/module.md)
* 五、[SBOM导入流程](doc/import_sbom/importSbom.md)
* 六、[特性清单](doc/features/features.md)
* 七、[运行](doc/run/howToRun.md)
* 八、API文档
  1. [查询制品SBOM看板统计数据](doc/api/查询制品SBOM看板统计数据.md)
  2. [查询制品漏洞列表](doc/api/查询制品漏洞列表.md)
  3. [查询制品license列表](doc/api/查询制品license列表.md)
  4. [查询制品软件包列表](doc/api/查询制品软件包列表.md)
  5. [查询软件包漏洞统计数据](doc/api/查询软件包漏洞统计数据.md)
  6. [查询软件包漏洞详情](doc/api/查询软件包漏洞详情.md)
  7. [查询软件包license、copyright详情](doc/api/查询软件包license、copyright详情.md)
  8. [查询软件包正向依赖项](doc/api/查询软件包正向依赖项.md)
  9. [查询软件包上游社区及Patch信息](doc/api/查询软件包上游社区及Patch信息.md)
  10. [反向追溯链查询](doc/api/反向追溯链查询.md)
  11. [漏洞影响范围查询](doc/api/漏洞影响范围查询.md)
  12. [查询制品配置](doc/api/查询制品配置.md)
  13. [制品发布](doc/api/制品发布.md)
  14. [查询制品发布结果](doc/api/查询制品发布结果.md)
  15. [新增制品元数据](doc/api/新增制品元数据.md)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
