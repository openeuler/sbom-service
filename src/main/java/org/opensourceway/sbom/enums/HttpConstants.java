package org.opensourceway.sbom.enums;

/**
 * ak加密
 *
 * @author ly
 * @since 2021-10-22
 */
public class HttpConstants {
    /**
     * 错误信息
     */
    public static final String ERROR_MESSAGE = "error_message";

    /**
     * 内容长度
     */
    public static final String CONTENT_LENGTH = "Content-Length";

    /**
     * 内容类别
     */
    public static final String CONTENT_TYPE = "Content-Type";

    /**
     * 内容格式
     */
    public static final String CONTENT_TYPE_VALUE = "application/json;charset=UTF-8";

    /**
     * error
     */
    public static final String ERROR = "error";

    /**
     * success
     */
    public static final String SUCCESS = "success";

    /**
     * access_token
     */
    public static final String ACCESS_TOKEN = "access_token";
}
