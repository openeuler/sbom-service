package org.opensourceway.sbom.enums;

/**
 * 功能描述
 *
 * @author ly
 * @since 2021-6-22
 */
public class PermissionConstants {
    /**
     * id
     */
    public static final String ID = "id";

    /**
     * token
     */
    public static final String TOKEN = "token";

    /**
     * name
     */
    public static final String NAME = "name";

    /**
     * avatar_url
     */
    public static final String AVATAR_URL = "avatar_url";

    /**
     * email
     */
    public static final String EMAIL = "email";

    /**
     * userId
     */
    public static final String USER_ID = "userId";

    /**
     * userName
     */
    public static final String USER_NAME = "userName";

    /**
     * login
     */
    public static final String LOGIN = "login";

    /**
     * access_token
     */
    public static final String ACCESS_TOKEN = "access_token";

    /**
     * sub
     */
    public static final String SUB = "sub";

    /**
     * ip
     */
    public static final String IP = "ip";

}
